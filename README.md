## Introduction

Le logo est fait de la manière suivante : on crée une image de très grande résolution pour conserver un maximum de détails, puis on vectorise cette image et l'on l'exporte au format PDF.

Je donne ici la manière avec laquelle je me suis pris pour réaliser le logo, mais il n'est évidemment pas exclus de s'y prendre autrement afin de l'embellir. Juste une consigne : ne pas utiliser de Comic Sans MS (pitié).

Il y a bien sûr deux logos à différencier : le logo MPSI et le logo MP. À la base, le logo MPSI est censé servir pour les deux années MPSI-MP. Comme "MP" revient deux fois et "SI" seulement une fois, on a choisi de laisser le "SI" deux fois plus petit que la lettre P. Le fait que "si" soit en minuscule n'est qu'une histoire d'esthétisme.
Néanmoins, l'année suivant la première distribution de sweats, les MP ont décidé de commander des sweats "MP" uniquement, juste pour la dernière année.

## Téléchargement

Sur [mega.nz](https://mega.nz/#!NGgkkQwb!IbgnVEFq98vY1RfwZFHfJB2umKKtpTekPij7KC1cpEg) (65 Mo)

## À propos du format

J'ai pour ma part reproduit le logo sur le logiciel Paint.NET (pour Windows seulement). Les fichiers de projet sont disponibles dans ce ZIP et contiennent différents calques. Au cas où cela ne conviendrait pas, les logos sont également disponibles en très grande résolution en PNG avec transparence. Les éléments du logo ne se chevauchant pas, il est assez facile de les isoler.
Par « très grande résolution », j'entends un carré de plusieurs milliers de pixels de côté. Mon image est de taille 10000x10000, ce qui est assez lourd... prévoir une bonne carte graphique pour ouvrir le fichier, donc. C'est d'ailleurs peut-être même un peu trop abusé, et je pense que du 5000x5000 suffit. C'est toujours beaucoup mais on divise la taille par 4. J'ai laissé les deux tailles.


## Les polices de caractères

Toutes les lettres grecques (sauf la lettre thêta), le m, le P, et le "si" sont écrites avec du Computer Modern Serif (ou CMU Serif). C'est la police par défaut en LaTeX.
Lien : https://sourceforge.net/projects/cm-unicode/
"MPSI" est écrit en italique

Le M fait référence au moment d'une force : il s'agit d'un "m" minuscule dont les traits ont été rallongés. Ne pas hésiter à faire mieux que moi pour mieux lisser le dernier trait qui descend jusqu'en dessous du P : si on regarde bien, il y a une sorte de discontinuité dans la courbure vers le haut. Ce n'est pas très grave si ce n'est pas corrigé, parce que ça ne saute vraiment pas aux yeux.

Comme dit plus haut, le "si" est en minuscule mais M. Morin le préfère en majuscule.

Le thêta est écrit en Times New Roman, parce que celui du CMU Serif est un peu moche.

Ne pas hésiter non plus à mieux organiser les lettres grecques, j'avoue ne pas les avoir placées très rigoureusement (mais, encore une fois, ce n'est pas dramatique).

"AR Lesage" est écrit en Script MT Bold.
Téléchargement : https://www.dafontfree.net/freefonts-script-mt-bold-f141949.htm

L'année est écrite en Castellar et en gras.
Téléchargement : https://www.dafontfree.net/freefonts-castellar-f117539.htm
Attention ! Il y a par défaut des espaces dans les caractères écrits. Je les ai rebouchés en noir.

Je n'ai pas noté les tailles, mais elles se retrouvent en effectuant des ajustements et en les comparant avec les lettres déjà présentes.


## La flèche, tout sur la flèche

La flèche est réalisée en latex. Pour en obtenir une image, je suis allé sur le site http://latex2png.com/ et j'ai entré le code \rightarrow pour avoir une flèche vers la droite. J'ai réglé la résolution au max, c'est-à-dire 2000. J'ai ensuite ajusté sa taille, puis j'ai allongé son trait pour l'adapter au logo.


## Vectorisation

Une fois l'image prête (préparer un grand PNG), il faut la vectoriser. J'ai confié cette tâche au logiciel Adobe Illustrator. Il est payant, et c'est clair qu'on a pas vraiment envie de l'acheter juste pour vectoriser un logo par an. Heureusement, on trouve des cracks sans trop de difficulté sur des sites de téléchargement comme YGG-Torent.
Des tutoriels de vectorisation sont disponibles sur YouTube par exemple, et c'est très bien expliqué (en réalité ce n'est pas bien compliqué, il y a un gros bouton "VECTORISER L'IMAGE" juste en haut quand celle-ci est sélectionnée). Le logo étant volumineux, l'opération peut durer plus d'une minute. On retrouve finalement l'image vectorisée en plusieurs morceaux.
Cependant, même si l'image PNG était transparente, Illustrator va rendre le fond opaque après la vectorisation. Il faut alors sélectionner tous les morceaux "de fond" et les supprimer (avec la touche Suppr). Vérifier qu'il n'en reste pas dans les feuilles de de la couronne.
Pour terminer, il suffit d'exporter le fichier en PDF. C'est le format réclamé par les vendeurs de sweats.


Pour toute information :
fabien.bernier@orange.fr